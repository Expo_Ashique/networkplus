# NetworkPlus App API

## Base url: http://157.7.242.91:8010/api/
 
By using the following endpoint, URL is formed by baseurl + endpoint and API communication is performed.
  
## Main endpoints

| Endpoint name |  Link  | Method |  Purpose | DOC-CHECK |
|---|---|---|---|---|
|  Registration | /registration  |POST | Registartion for user | OK | 
|  Forget Password | /forget-password    | POST |  For Forget Password  | OK | 
|  Reset Password | /reset-password    | POST |  For Reset Password  | OK |
|  Login | /login    | POST |  For User Login  | OK |
|  Logout | /logout-user   | GET|  For User Logout | OK |
|  User | /user/< user_id >   | GET|  For User Details | OK |
|  User | /get-me   | GET|  For Getting Own Profile Information | OK |
|  Profile | /update-profile   | GET|  For Updating User Details | OK |
|  Follower | /follower   | GET|  For Adding New Follower | OK |
|  Follower | /get-follow-list   | GET|  For A Single User Follow List  | OK |
|  Search | /search   | GET|  For Searching User  | OK |
|  Notification | /get-notifications   | GET|  For User Notification | OK |
|  NearBy User | /get-nearby   | GET|  For Getting Nearby User | OK |






## Endpoint detail:
 

###HTTP REQUEST :  **POST  /registration**

###### params
```json 
{
   "profile_image": "content: base64-image-string",
   "username":"Inzamamul",
   "password":"1234",
   "email":"ashique@gmail.com"
}
```

| parameter | is required | comment |
| :---------: | :---: | :-----------: | :-------: | :----------- |
| profile_image      | true | |
| username      | true | |
| password      | true |  Encrypted in sha256 formated |
| email      | true |  |

 
###### output
``` json 
{
    "message": "Registration Completed.",
    "data": {
        "profile_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique4_20.jpeg",
        "email": "ashiq0ooo@gmail.com",
        "id": 20,
        "session": "4dd9b5c8-9ebf-45b0-a5fc-231d77f01285",
        "username": "Expo_Ashique4"
    },
    "status": "success"
}

```
 

###HTTP REQUEST :  **POST  /forget-password**

###### params
```json 
{
   "email":"ashique@gmail.com"
}
```

| parameter | is required | comment |
| :---------: | :---: | :-----------: | :-------: | :----------- | 
| email      | true |  |

 
###### output
``` json 
{
    "message": "ok.",
    "data": {
        "security_code": "2a9f7b6e-5272-4678-a61b-6fd898760856",
        "email": "ashique00003@gmail.com"
    },
    "status": "sucess"
}
```



###HTTP REQUEST :  **POST  /reset-password**

###### params
```json 
{
     "security_code":"696146fc-59a7-4773-bfca-0b74e64853cc",
     "new_password": "kjhlkzdf"
}
```

| parameter | is required | comment |
| :---------: | :---: | :-----------: | :-------: | :----------- | 
| email      | true |  |
| new_password      | true |  |

 
###### output
``` json 
{
   "data":{

   },
   "message":"Password reset Successful.",
   "status":"success"
}
```




###HTTP REQUEST :  **POST  /login**

###### params

```json
{
     "email": "ashique@gmail.com",
     "password": "dfsfsdfdsgdfgdgf"
}
```

| parameter | is required | comment|
| :---------: | :---: | :-----------: | :-------: | :----------- |
| email      | true |  |
| password      | true | Encrypted in sha256 formated |   

 
###### output 

```json
{
    "message": "Valid Login",
    "data": {
        "profile_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique_6.jpeg",
        "email": "",
        "username": "ashique00003@gmail.com",
        "id": 6,
        "session": "cf5b1953-8b4c-4450-885e-e99f3a102519"
    },
    "status": "success"
}
```

### HTTP REQUEST :  **POST /logout-user**

###### params
```json
{
     "session": "0vvqqc47gyd0c1ediobu1iuxk596d6qd"      
}
```

| parameter | is required | 
| :---------: | :---: | :-----------: | :-------: | :----------- |
| session      | true | 
  

 
###### output

```json
{
   "message":"Logged Out",
   "status":"success"
}
```
     

### HTTP REQUEST :  **GET /user/< pk >/**
###Exaple URL : http://127.0.0.1:8000/api/user/6/?session=mnwdfqw470wzojq005z4mfnozq4ea01y

###### params
```json
{
     "session": "0vvqqc47gyd0c1ediobu1iuxk596d6qd"      
}
```

| parameter | is required | 
| :---------: | :---: | :-----------: | :-------: | :----------- |
| session      | true | 
  

 
###### output

```json
{
    "message": "okay",
    "data": [
        {
            "longitude": "-74.0088189",
            "username": "Inzamamul Haque2",
            "id": 6,
            "position": "Jr. Software Engineer2",
            "company": "DDS6",
            "industry":"IT",
            "latitude": "40.70600088",
            "email": "ashique00003@gmail.com",
            "state_or_country": "Bangladesh2",
            "facebook_id": "facebook_ashique2",
            "skype_id": "expo_ashique2",
            "profile_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique_6.jpeg"
        }
    ],
    "status": "success"
}
```

### HTTP REQUEST :  **GET /get-me**
###Exaple URL : http://127.0.0.1:8000/api/get-me/?session=mnwdfqw470wzojq005z4mfnozq4ea01y


###### params
```json
{
     "session": "0vvqqc47gyd0c1ediobu1iuxk596d6qd"      
}
```

| parameter | is required | 
| :---------: | :---: | :-----------: | :-------: | :----------- |
| session      | true | 
  

 
###### output

```json
{
    "message": "okay",
    "data": [
        {
            "longitude": "-74.0088189",
            "username": "Inzamamul Haque2",
            "id": 6,
            "position": "Jr. Software Engineer2",
            "company": "DDS6",
            "latitude": "40.70600088",
            "email": "ashique00003@gmail.com",
            "state_or_country": "Bangladesh2",
            "facebook_id": "facebook_ashique2",
            "skype_id": "expo_ashique2",
            "profile_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique_6.jpeg"
        }
    ],
    "status": "success"
}
```



###HTTP REQUEST :  **POST  /update-profile**

###### params
```json 
{
	"session":"mnwdfqw470wzojq005z4mfnozq4ea01y",
	"username":"Inzamamul Haque2",
	"company":"DDS6",
	"industry":"IT"
	"position":"Jr. Software Engineer2",
	"state_or_country":"Bangladesh2",
	"phone":"+88016256924892",
	"website":"www.google.com/expo_ashique2/",
	"address":"Mirpur-12 ,Dhaka2",
	"skype_id":"expo_ashique2",
	"twitter_id":"twitter_ashique2",
	"facebook_id":"facebook_ashique2",
	"latitude":"40.70600088",
	"longitude":"-74.0088189" 
}
```

| parameter | is required | comment |
| :---------: | :---: | :-----------: | :-------: | :----------- | 
| session      | true |  |
| username      | false |  |
| company      | false |  |
| position      | false |  |
| state_or_country      | false |  |
| phone      | false |  |
| website      | false |  |
| address      | false |  |
| skype_id      | false |  |
| twitter_id      | false |  |
| facebook_id      | false |  |
| latitude      | false |  |
| longitude      | false |  |

 
###### output
``` json 
{
    "message": "Succesfully Updated.",
    "data": {
        "username": "Inzamamul Haque2",
        "address": "Mirpur-12 ,Dhaka2",
        "website": "www.google.com/expo_ashique2/",
        "company": "DDS6",
        "industry":"IT",
        "state_or_country": "Bangladesh2",
        "email": "ashique00003@gmail.com",
        "phone": "+88016256924892",
        "longitude": "-74.0088189",
        "facebook_id": "facebook_ashique2",
        "id": 6,
        "twitter_id": "twitter_ashique2",
        "position": "Jr. Software Engineer2",
        "profile_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique_6.jpeg",
        "skype_id": "expo_ashique2",
        "latitude": "40.70600088"
    },
    "status_code": "200",
    "status": "success"
}

```

###HTTP REQUEST :  **POST  /follower**

###### params
```json 
{
   "is_follow": "1",   (1 for add follower and 0 for remove follower)
   "following_user_id":"12",
   "session":"mnwdfqw470wzojq005z4mfnozq4ea01y" 
}

```

| parameter | is required | comment |
| :---------: | :---: | :-----------: | :-------: | :----------- |
| is_follow      | true | |
| following_user_id      | true | |
| session      | true |  Encrypted in sha256 formated | 

 
###### output
``` json 
{
    "message": "Follower added",
    "status": "success"
}
```



###HTTP REQUEST :  **POST  /get-follow-list**

###### params
```json 
{
   "session":"mnwdfqw470wzojq005z4mfnozq4ea01y" ,
   "user_id":"11",
   "call_type":"follower",   (or 'following')
   "email":"ashique@gmail.com",
   "page":"3"
}
```

| parameter | is required | comment |
| :---------: | :---: | :-----------: | :-------: | :----------- |
| profile_image      | true | |
| username      | true | |
| password      | true |  Encrypted in sha256 formated |
| email      | true |  |
| page      | flase | | 

 
###### output
``` json 
{
    "message": "ok",
    "data": [
        {
            "follower_distance_in_km": 0.000012258839578730288,
            "follower_username": "Shaekhh",
            "added_date": "2018-04-04 00:00:00",
            "follower_email": "ashique003@gmail.com",
            "follower_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique3_16.jpeg",
            "follower_id": 16
        },
        {
            "follower_distance_in_km": 3342.8257695719644,
            "follower_username": "ruhul ossain",
            "added_date": "2018-04-29 00:00:00",
            "follower_email": "ashiqu03@gmail.com",
            "follower_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique3_18.jpeg",
            "follower_id": 18
        },
        {
            "follower_distance_in_km": 0.00003488190527212779,
            "follower_username": "Jakson mahmud",
            "added_date": "2018-04-03 00:00:00",
            "follower_email": "ashiq03@gmail.com",
            "follower_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique3_19.jpeg",
            "follower_id": 19
        }
    ],
    "total_count": 3,
    "page": 1,
    "status": "success"
}

```


###HTTP REQUEST :  **POST  /search**

###### params
```json 
{
   "keyword": "ashiq" ,
   "page":"2"
}

```

| parameter | is required | comment |
| :---------: | :---: | :-----------: | :-------: | :----------- |
| keyword      | true | | 
| page      | flase | | 

 
###### output
``` json 
{
    "message": "",
    "data": {
        "page": 1,
        "user": {
            "results": [
                {
                    "profile_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique_6.jpeg",
                    "email": "ashique00003@gmail.com",
                    "username": "Inzamamul Haque2",
                    "id": 6
                },
                {
                    "profile_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique3_16.jpeg",
                    "email": "ashique003@gmail.com",
                    "username": "Shaekhh",
                    "id": 16
                },
                {
                    "profile_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique3_18.jpeg",
                    "email": "ashiqu03@gmail.com",
                    "username": "ruhul ossain",
                    "id": 18
                },
                {
                    "profile_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique3_19.jpeg",
                    "email": "ashiq03@gmail.com",
                    "username": "Jakson mahmud",
                    "id": 19
                },
                {
                    "profile_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique4_20.jpeg",
                    "email": "ashiq0ooo@gmail.com",
                    "username": "Expo_Ashique4",
                    "id": 20
                }
            ]
        },
        "total_count": 5
    },
    "status": "success"
}

```




###HTTP REQUEST :  **POST  /get-notifications**

###### params
```json 
{
   "user_id": "2", 
   "page":"1"
}
```

| parameter | is required | comment |
| :---------: | :---: | :-----------: | :-------: | :----------- |
| user_id      | true | | 
| page      | true |  |

 
###### output
``` json 
{
    "message": "ok",
    "data": [
        {
            "is_nearby": [
                {
                    "distance_in_km": 0.00003488190527212779,
                    "follower_username": "Inzamamul Haque2",
                    "follower_id": 6,
                    "follower_email": "ashique00003@gmail.com",
                    "following_user_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique_6.jpeg"
                }
            ],
            "follow_notifications": [],
            "change_profile_data": [
                {
                    "is_notified": true,
                    "added_date": "2018-05-04 14:39:24",
                    "notification_owner": 19,
                    "user_id": 6,
                    "notification_type": "company"
                },
                {
                    "is_notified": true,
                    "added_date": "2018-05-03 14:46:59",
                    "notification_owner": 19,
                    "user_id": 6,
                    "notification_type": "address"
                },
                {
                    "is_notified": true,
                    "added_date": "2018-05-03 14:46:59",
                    "notification_owner": 19,
                    "user_id": 6,
                    "notification_type": "facebook_id"
                },
                {
                    "is_notified": true,
                    "added_date": "2018-05-03 14:46:58",
                    "notification_owner": 19,
                    "user_id": 6,
                    "notification_type": "twitter_id"
                },
                {
                    "is_notified": true,
                    "added_date": "2018-05-03 14:46:58",
                    "notification_owner": 19,
                    "user_id": 6,
                    "notification_type": "skype_id"
                },
                {
                    "is_notified": true,
                    "added_date": "2018-05-03 14:46:58",
                    "notification_owner": 19,
                    "user_id": 6,
                    "notification_type": "address"
                },
                {
                    "is_notified": true,
                    "added_date": "2018-05-03 14:46:58",
                    "notification_owner": 19,
                    "user_id": 6,
                    "notification_type": "website"
                },
                {
                    "is_notified": true,
                    "added_date": "2018-05-03 14:46:58",
                    "notification_owner": 19,
                    "user_id": 6,
                    "notification_type": "phone number"
                },
                {
                    "is_notified": true,
                    "added_date": "2018-05-03 14:46:58",
                    "notification_owner": 19,
                    "user_id": 6,
                    "notification_type": "state_or_country"
                },
                {
                    "is_notified": true,
                    "added_date": "2018-05-03 14:46:57",
                    "notification_owner": 19,
                    "user_id": 6,
                    "notification_type": "position"
                }
            ]
        }
    ],
    "total_count": 0,
    "page": "1",
    "status": "success"
}

```
 
###HTTP REQUEST :  **POST  /get-nearby**

###### params
```json 
{
 
   "user_id":"22" (self)
}
```

| parameter | is required | comment |
| :---------: | :---: | :-----------: | :-------: | :----------- |
| user_id      | true | | 

 
###### output
``` json 
{
    "message": "ok",
    "data": [
        {
            "is_nearby": [
                {
                    "distance_in_km": 0.00003488190527212779,
                    "follower_username": "Inzamamul Haque2",
                    "follower_id": 6,
                    "follower_email": "ashique00003@gmail.com",
                    "following_user_image": "http://127.0.0.1:8000/media/profile-picture/Expo_Ashique_6.jpeg"
                }
            ]
        }
    ],
    "status": "success"
}status": "success"
}

```