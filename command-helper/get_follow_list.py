import socket
import sys
from threading import Thread
import traceback
import json
import requests

#for getting follow-list or following-list you can use same url ... you just need to change call_type
# as 'follower' or 'following'  ...


URL = "http://127.0.0.1:8001/api/get-follow-list/"

data = {}
data["session"]="1am9x5qjcks2hnacg64q0b196s1d34m2"
data["user_id"]="2"
data["call_type"]="follower"   # or following
data["page"]="1"

response = requests.get(URL, params = data,headers={'Authorization':'1am9x5qjcks2hnacg64q0b196s1d34m2'})

print(response.json())

if response.status_code == 200:
    response = response.json()
    # print(response)
