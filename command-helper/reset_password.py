import socket
import sys
from threading import Thread
import traceback
import json
import requests
import hashlib

#for reseting password you have to use security_code fron 'forget_password' command helper


URL = "http://127.0.0.1:8001/api/reset-password/"

data = {}
data["security_code"]="6389a2e0-7791-4900-81ba-30e61836631f"
data["new_password"]=hashlib.sha256("1234".encode()).hexdigest()

response = requests.post(URL, data = data,headers={'Authorization':'1am9x5qjcks2hnacg64q0b196s1d34m2'})

print(response.json())

if response.status_code == 200:
    response_json = response.json()
    # print(response)
